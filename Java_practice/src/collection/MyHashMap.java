package collection;

public class MyHashMap<k,v> {

	Entity<k,v> table[];
	int capacity;
	
	static class Entity<k,v>{
		k key;
		v value;
		Entity<k,v> next;
		
		public Entity(k key,v value,Entity<k,v> next){
			this.key=key;
			this.value=value;
			this.next=next;
			}
	}
	
	
	public MyHashMap(int capacity){
		this.capacity=capacity;
		table=new Entity[capacity];
		}
	
	private int hash(k key){
		return Math.abs(key.hashCode())%this.capacity;
	}
	
	//
	public void put(k key,v value){
		if(key == null)
			return;
		int index=hash(key);
		Entity<k,v> obj=new Entity(key,value,null);
		if(table[index]==null){
			table[index]=obj;
		}
		else{
			Entity<k,v> head= table[index];
		    Entity<k,v> pre=null;  
			
			while(head!=null){
			if(head.key.equals(key)){
			head.value=value;
			return;
			}
			pre=head;
			head=head.next;
			}
		pre.next=obj;	
		}
		}
	
	public v get(k key){
		int index=hash(key);
		Entity<k,v> head=table[index];
		if(table[index]==null){
			return null;
		}
		while(head!=null){
			if(key.equals(head.key)){
				return head.value;
			}
		head=head.next;
		}
		
		return null;
	}
	
	
	public boolean delete(k key){
		int index=hash(key);
		Entity<k,v> head=table[index];
		Entity<k,v> pre=null;
		
		if(table[index]==null){
			return false;
		}
		
		while(head!=null){
			if(key.equals(head.key)){
				if(pre==null){
					table[index]=table[index].next;
					return true;
				}
				else{
				pre.next=head.next;
				return true;
				}
			}
			
			pre=head;
			head=head.next;
		}
		
		return false;
		
	}
	
	public void display(){
	
	for(int i=0;i<capacity;i++){
		if(table[i]!=null){
		Entity<k,v> ent=table[i];
		while(ent!=null){
				System.out.println("key"+ent.key  +"value"+ent.value);
				ent=ent.next;
			}
		}
				
		
	}
		
	}
	
	
	
	public static void main(String[] args) {
		
		MyHashMap<Integer, Integer> hm =new MyHashMap<Integer,Integer>(5);
		hm.put(1, 10);
		hm.put(2, 20);
		hm.put(1, 11);
		hm.put(5, 17);
		System.out.println(hm.delete(5));
		System.out.println(hm.get(2));
		hm.display();
		
	}
	
	
	
	
}

